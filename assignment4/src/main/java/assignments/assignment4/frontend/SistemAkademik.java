package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class SistemAkademik {

    public static void main(String[] args) { 
        new SistemAkademikGUI();
    }
}

class SistemAkademikGUI extends JFrame{
    private static ArrayList<Mahasiswa> daftarMahasiswa = new ArrayList<Mahasiswa>();
    private static ArrayList<MataKuliah> daftarMataKuliah = new ArrayList<MataKuliah>();
    public static Font fontGeneral = new Font("Segoe UI", Font.PLAIN , 16);
    public static Font fontTitle = new Font("Segoe UI", Font.BOLD, 25);

    // Custom colors and fonts
    public static Color whiteText = new Color(250,250,250);
    public static Color greenButton = new Color(73, 190, 170);
    public static Color blueButton = new Color(132, 174, 204);
    public static String backgroundPanel = "#EAF9D9";
    
    public SistemAkademikGUI(){

        // Membuat Frame
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);
        frame.setResizable(true);

        frame.setTitle("Administrator - Sistem Akademik");
        frame.setLocationRelativeTo(null);  // Centers the window
        
        
        new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
        frame.setVisible(true);

    }
}

// Sorting Class
/*
    Use insertion sort methodology

    Sources : 
    https://www.educative.io/edpresso/what-is-insertion-sort-in-java
    https://www.geeksforgeeks.org/insertion-sort/ 
*/

class InsertionSorting{

    /*  Sort the array in ascending order
        Utilize the toString method in Mahasiswa and Matakuliah class

        toString in Mahasiswa returns Mahasiswa.npm in String form
        toString in Matakuliah returns Matakuliah.nama in String

        return the sorted version of sortList */
    public static <T> ArrayList<T> sortArrayList (ArrayList<T> sortList) {
        // Outer Loop
        // Starts from index no.1
        for(int i=1;i<sortList.size();i++){

            int j = i;  
            T key = sortList.get(i); 
   
            /*      Inner Loop
                Swap the elements until the key is already bigger
                than the element to its left

                Value of T.compareTo > 0 indicates that the first element
                is greater than the second
            */
            while(j > 0 && (sortList.get(j-1).toString().compareTo(key.toString()) > 0)){

                // Swap elements
                sortList.set(j, sortList.get(j-1)) ;
                sortList.set(j-1, key);
                j = j-1; 
            }
        }
        return sortList;
    }

    
}

/* In collaboration with :
-Alya Muthia Fitri
-Andrea Debora Narulita
-Atika Najla Febriyani
-Raissa Adlina Febrianny
*/