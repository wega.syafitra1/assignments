package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;

// import org.graalvm.compiler.lir.CompositeValue.Component;

import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI{

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        frame.setSize(500,600);

        // JPanel to put every Components on
        JPanel contents = new JPanel();
        contents.setLayout(new BoxLayout(contents, BoxLayout.Y_AXIS));
        contents.setBackground(Color.decode(SistemAkademikGUI.backgroundPanel));

        // Set up the title 
        JLabel titleLabel = new JLabel();
        titleLabel.setText(" Tambah Mata Kuliah ");
        titleLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);  


        // Set up "Nama Mata Kuliah" label and its input field 
        JLabel nameLabel = new JLabel();
        nameLabel.setText("Nama Mata Kuliah: ");
        nameLabel.setHorizontalAlignment(JLabel.CENTER);
        nameLabel.setFont(SistemAkademikGUI.fontGeneral); 
        nameLabel.setAlignmentX(Component.CENTER_ALIGNMENT);  // Center-align the label in X axis

        JTextField namaField = new JTextField(20);
        namaField.setMaximumSize(namaField.getPreferredSize());
        namaField.setAlignmentX(Component.CENTER_ALIGNMENT);
         
        // Set up "Kode Mata Kuliah" label and its input field 
        JLabel kodeLabel = new JLabel();
        kodeLabel.setText("Kode Mata Kuliah: ");
        kodeLabel.setHorizontalAlignment(JLabel.CENTER);
        kodeLabel.setFont(SistemAkademikGUI.fontGeneral); 
        kodeLabel.setAlignmentX(Component.CENTER_ALIGNMENT);  

        JTextField kodeField = new JTextField(20);
        kodeField.setMaximumSize(kodeField.getPreferredSize());
        kodeField.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Set up "SKS" label and its input field 
        JLabel sksLabel = new JLabel();
        sksLabel.setText("SKS: ");
        sksLabel.setHorizontalAlignment(JLabel.CENTER);
        sksLabel.setFont(SistemAkademikGUI.fontGeneral); 
        sksLabel.setAlignmentX(Component.CENTER_ALIGNMENT);  

        JTextField sksField = new JTextField(20);
        sksField.setMaximumSize(sksField.getPreferredSize());
        sksField.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Set up "Kapasitas" label and its input field 
        JLabel kapLabel = new JLabel();
        kapLabel.setText("Kapasitas: ");
        kapLabel.setHorizontalAlignment(JLabel.CENTER);
        kapLabel.setFont(SistemAkademikGUI.fontGeneral); 
        kapLabel.setAlignmentX(Component.CENTER_ALIGNMENT);  

        JTextField kapField = new JTextField(20);
        kapField.setMaximumSize(kapField.getPreferredSize());
        kapField.setAlignmentX(Component.CENTER_ALIGNMENT);
        

        // Set up "Tambahkan" button and its interactions
        JButton addButton = new JButton("Tambahkan");
        addButton.setFont(SistemAkademikGUI.fontGeneral);
        addButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        addButton.setBackground(SistemAkademikGUI.greenButton);
        addButton.setForeground(SistemAkademikGUI.whiteText);


        // Adds a MataKuliah instance to daftarMataKuliah
        
        addButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){

                String matkulNama = namaField.getText();  // Nama attribute for mahasiswa instance
                String kode = kodeField.getText();  // Kode attribute
                String sks = sksField.getText();  // NPM attribute
                String kapasitas = kapField.getText(); // Kapasitas attribute

                // Validate if atleast one field is empty
                if (kode.length() == 0 || matkulNama.length() == 0 || sks.length() == 0 || kapasitas.length() == 0){
                    JOptionPane.showMessageDialog(null,"Mohon isi seluruh Field");
                }
                
                // Validate if there is already matakuliah instance with the same name
                boolean inserted = false;
                for (MataKuliah matkul : daftarMataKuliah){
                    if (matkul.getNama().equals(matkulNama)) {
                        inserted = true;
                    }
                }

                if (inserted){
                    JOptionPane.showMessageDialog(null,String.format("Mata Kuliah %s sudah pernah ditambahkan sebelumnya",matkulNama));
                    kodeField.setText("");
                    namaField.setText("");
                    sksField.setText("");
                    kapField.setText("");
                } else {
                    int sksINT = Integer.parseInt(sks);
                    int kapINT = Integer.parseInt(kapasitas);
                    daftarMataKuliah.add(new MataKuliah(kode, matkulNama, sksINT, kapINT));
                    JOptionPane.showMessageDialog(null,String.format("Mata Kuliah %s berhasil ditambahkan", matkulNama));
                    // Reset textfield
                    kodeField.setText("");
                    namaField.setText("");
                    sksField.setText("");
                    kapField.setText("");
                    
                }
            }
        });


        // Set up "Kembali" button and its interactions
        JButton backButton = new JButton("Kembali");
        backButton.setFont(SistemAkademikGUI.fontGeneral);
        backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        backButton.setBackground(SistemAkademikGUI.blueButton);
        backButton.setForeground(SistemAkademikGUI.whiteText);

        // Removes all content/components from current frame
        // and store components from HmeGUI
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){

                frame.setSize(500,500);
                frame.getContentPane().removeAll();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            }
        });


        // Add the components to JPanel contents 
        contents.add(Box.createRigidArea(new Dimension(0,75)));
        contents.add(titleLabel);
       
        contents.add(Box.createRigidArea(new Dimension(0,30)));

        contents.add(kodeLabel);
        contents.add(Box.createRigidArea(new Dimension(0,10)));

        contents.add(kodeField);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        
        contents.add(nameLabel);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        
        contents.add(namaField);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        
        contents.add(sksLabel);
        contents.add(Box.createRigidArea(new Dimension(0,10)));

        contents.add(sksField);
        contents.add(Box.createRigidArea(new Dimension(0,10)));

        contents.add(kapLabel);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        
        contents.add(kapField);
        contents.add(Box.createVerticalStrut(20));
        
        contents.add(addButton);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        
        contents.add(backButton);

        frame.add(contents);
        frame.setVisible(true);


    }
    
}
