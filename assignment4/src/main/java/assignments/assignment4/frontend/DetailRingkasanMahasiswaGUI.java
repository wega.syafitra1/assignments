package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;


public class DetailRingkasanMahasiswaGUI {
    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        frame.setSize(500,500);
        // JPanel to put every Components on
        JPanel contents = new JPanel();
        contents.setLayout(new BoxLayout(contents, BoxLayout.Y_AXIS));
        contents.setBackground(Color.decode(SistemAkademikGUI.backgroundPanel));

        // Access the attributes of mahasiswa object
        String nama = mahasiswa.getNama();
        long npm = mahasiswa.getNpm();
        String jurusan = mahasiswa.getJurusan();
        int sks = mahasiswa.getTotalSKS();
        mahasiswa.cekIRS();
        
        // Set up the title 
        JLabel titleLabel = new JLabel();
        titleLabel.setText(" Detail Ringkasan Mahasiswa ");
        titleLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        // Set up "Nama" label
        JLabel namaLabel = new JLabel();
        namaLabel.setText(String.format("Nama: %s", nama));  // nama of mahasiswa obj
        namaLabel.setFont(SistemAkademikGUI.fontGeneral);
        namaLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Set up "NPM" label
        JLabel npmLabel = new JLabel();
        npmLabel.setText(String.format("NPM: %d", npm));  // npm of mahasiswa obj
        npmLabel.setFont(SistemAkademikGUI.fontGeneral);
        npmLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Set up "Jurusan" label
        JLabel jurusanLabel = new JLabel();
        jurusanLabel.setText(String.format("Jurusan: %s", jurusan));  // jurusan of mahasiswa obj
        jurusanLabel.setFont(SistemAkademikGUI.fontGeneral);
        jurusanLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Set up "Daftar Mata Kuliah" label and the list of matakuliah that has been taken
        JLabel dafMatkulLabel = new JLabel();
        dafMatkulLabel.setText("Daftar Mata Kuliah:");
        dafMatkulLabel.setFont(SistemAkademikGUI.fontGeneral);
        dafMatkulLabel.setAlignmentX(Component.CENTER_ALIGNMENT);  

        // Panel to hold the matakuliah that will be listed
        JPanel dafMatkul = new JPanel();
        dafMatkul.setLayout(new BoxLayout(dafMatkul, BoxLayout.Y_AXIS));

        // Font for label of listing matakul.nama and IRS Problem
        Font labelListFont = new Font("Century Gothic", Font.BOLD, 12);  
        

        // Validate if mahasiswa hasn't taken any matakuliah yet
        if (mahasiswa.getBanyakMatkul() == 0){
            JLabel noLabel = new JLabel("Belum ada mata kuliah yang diambil");
            noLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
            dafMatkul.add(noLabel);
        } else {
            
            // Prints all matakuliah in mahasiswa.daftarMataKuliah array
            for (int i = 0; i < mahasiswa.getBanyakMatkul(); i++){
                MataKuliah matkul = mahasiswa.getMataKuliah()[i];

                dafMatkul.add(Box.createRigidArea(new Dimension(0,7)));  // Top spacing

                // Create a new JLabel on each iteration
                // listLabel variable for temporary placement of mahasiswa.name
                // Add the new label to daftMhs along with its formatting
                JLabel listLabel = new JLabel(String.format("%d. %s\n", (i+1), matkul.getNama()));
                listLabel.setFont(labelListFont);
                listLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
                dafMatkul.add(listLabel);

                dafMatkul.add(Box.createRigidArea(new Dimension(0,7)));  // Bottom spacing
                 
            }
        }


        // Set up "Total SKS" label
        JLabel sksLabel = new JLabel();
        sksLabel.setText(String.format("Total SKS: %d", sks));  // sks of mahasiswa obj
        sksLabel.setFont(SistemAkademikGUI.fontGeneral);
        sksLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Set up "Hasil Pengecekan IRS" label and the list of IRS problem that occur
        JLabel irsLabel = new JLabel();
        irsLabel.setText("Hasil Pengecekan IRS:");
        irsLabel.setFont(SistemAkademikGUI.fontGeneral);
        irsLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Panel to hold IRS problems that will be listed
        JPanel daftIRS = new JPanel();
        daftIRS.setLayout(new BoxLayout(daftIRS, BoxLayout.Y_AXIS));

        // Validate if mahasiswa doesn't have any IRS problems
        if (mahasiswa.getBanyakMasalahIRS() == 0){

            // Create a new JLabel with its formatting
            // Add it to daftIRS panel
            JLabel listLabelIRS = new JLabel("IRS tidak bermasalah");
            listLabelIRS.setFont(labelListFont);
            listLabelIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
            daftIRS.add(listLabelIRS);
        } else {
            
            // Prints IRS problems
            for (int i = 0; i < mahasiswa.getBanyakMasalahIRS(); i++){
                String probs = mahasiswa.getMasalahIRS()[i];
                
                daftIRS.add(Box.createRigidArea(new Dimension(0,7)));  // Top spacing

                // Create a new JLabel with its formatting on every iteration
                JLabel listLabelIRS = new JLabel(String.format("%d. %s", (i+1),probs));
                listLabelIRS.setFont(labelListFont);
                listLabelIRS.setAlignmentX(Component.CENTER_ALIGNMENT);

                daftIRS.add(listLabelIRS);
                daftIRS.add(Box.createRigidArea(new Dimension(0,7)));  // Bottom spacing
                
            }
        }

        // Set up "Selesai" button and its interactions
        JButton backButton = new JButton("Selesai");
        backButton.setFont(SistemAkademikGUI.fontGeneral);
        backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        backButton.setBackground(SistemAkademikGUI.greenButton);
        backButton.setForeground(SistemAkademikGUI.whiteText);

        // Removes all content/components from current frame
        // and store components from HomeGUI
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){

                frame.getContentPane().removeAll();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            }
        });



        // Adds all components to JPanel contents
        contents.add(Box.createRigidArea(new Dimension(0,100)));
        contents.add(titleLabel);
        
        contents.add(Box.createRigidArea(new Dimension(0,15)));
        contents.add(namaLabel);

        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(npmLabel);

        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(jurusanLabel);

        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(dafMatkulLabel);

        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(dafMatkul);

        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(sksLabel);

        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(irsLabel);

        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(daftIRS);

        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(backButton);


        frame.add(contents);
        frame.setVisible(true);
    }
}
