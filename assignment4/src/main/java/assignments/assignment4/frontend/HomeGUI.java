package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HomeGUI extends JFrame {
    
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        frame.setSize(500,500);
        
        // JPanel to store all the Buttons and Labels
        JPanel placement = new JPanel();
        placement.setLayout(new BoxLayout(placement, BoxLayout.Y_AXIS));
        placement.setBackground(Color.decode(SistemAkademikGUI.backgroundPanel));

        
        // The title label for this page
        JLabel titleLabel = new JLabel();
        titleLabel.setText("   Selamat datang di Sistem Akademik  ");
        titleLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);  // Center-align the label in X axis
        
        // Initiation of the JButtons
        JButton tambahMahasiswa = new JButton("Tambah Mahasiswa");
        JButton tambahMatkul = new JButton("Tambah Mata Kuliah");
        JButton tambahIRS = new JButton("Tambah IRS");
        JButton hapusIRS = new JButton("Hapus IRS");
        JButton ringkasMHS = new JButton("Lihat Ringkasan Mahasiswa");
        JButton ringkasMatkul = new JButton("Lihat Ringkasan Mata Kuliah");
        
    
        // Set the JButtons fonts
        tambahMahasiswa.setFont(SistemAkademikGUI.fontGeneral);
        tambahMatkul.setFont(SistemAkademikGUI.fontGeneral);
        tambahIRS.setFont(SistemAkademikGUI.fontGeneral);
        hapusIRS.setFont(SistemAkademikGUI.fontGeneral);
        ringkasMHS.setFont(SistemAkademikGUI.fontGeneral);
        ringkasMatkul.setFont(SistemAkademikGUI.fontGeneral);
        
        // Set the JButtons alignments
        tambahMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        hapusIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        ringkasMHS.setAlignmentX(Component.CENTER_ALIGNMENT);
        ringkasMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Set the color of the buttons background
        tambahMahasiswa.setBackground(SistemAkademikGUI.greenButton);
        tambahMatkul.setBackground(SistemAkademikGUI.greenButton);
        tambahIRS.setBackground(SistemAkademikGUI.greenButton);
        hapusIRS.setBackground(SistemAkademikGUI.greenButton);
        ringkasMHS.setBackground(SistemAkademikGUI.greenButton);
        ringkasMatkul.setBackground(SistemAkademikGUI.greenButton);

        // Set the color of the buttons' text
        tambahMahasiswa.setForeground(SistemAkademikGUI.whiteText);
        tambahMatkul.setForeground(SistemAkademikGUI.whiteText);
        tambahIRS.setForeground(SistemAkademikGUI.whiteText);
        hapusIRS.setForeground(SistemAkademikGUI.whiteText);
        ringkasMHS.setForeground(SistemAkademikGUI.whiteText);
        ringkasMatkul.setForeground(SistemAkademikGUI.whiteText);


        // Append the buttons to the frame

        placement.add(Box.createRigidArea(new Dimension(0,75)));  // Inserts an invisible object for filler
        placement.add(titleLabel);
        placement.add(Box.createRigidArea(new Dimension(0,30)));
        placement.add(tambahMahasiswa);
        placement.add(Box.createRigidArea(new Dimension(0,10)));
        placement.add(tambahMatkul);   
        placement.add(Box.createRigidArea(new Dimension(0,10)));
        placement.add(tambahIRS);
        placement.add(Box.createRigidArea(new Dimension(0,10)));
        placement.add(hapusIRS);
        placement.add(Box.createRigidArea(new Dimension(0,10)));
        placement.add(ringkasMHS);
        placement.add(Box.createRigidArea(new Dimension(0,10)));
        placement.add(ringkasMatkul);

        frame.add(placement);

        // Implement ActionListener for every JButtons
        
        // ActionListener for tambahMahasiswa 
        // removes the current contentpane to store components from TambahMahasiswaGUI
        tambahMahasiswa.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){

                // Calls TambahMahasiswaGUI to overwrite the existing panel
                // so that no new window is called
                frame.getContentPane().removeAll();
                new TambahMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah); 
                
            }
        });
        // ActionListener for tambahMatkul
        tambahMatkul.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){

                frame.getContentPane().removeAll();
                new TambahMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            }
        });


        // ActionListener for tambahIRS
        tambahIRS.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){

                frame.getContentPane().removeAll();
                new TambahIRSGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            }
        });

        // ActionListener for hapusIRS
        hapusIRS.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){

                frame.getContentPane().removeAll();
                new HapusIRSGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            }
        });

        // ActionListener for ringkasMHS
        ringkasMHS.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){

                frame.getContentPane().removeAll();
                new RingkasanMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            }
        });


        // ActionListener for ringkasMatkul
        ringkasMatkul.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){

                frame.getContentPane().removeAll();
                new RingkasanMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            }
        });
        
        frame.setVisible(true);
    }
}
