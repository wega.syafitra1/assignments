package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;

//import org.graalvm.compiler.lir.CompositeValue.Component;

import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI {

    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        frame.setSize(500,500);

        // JPanel to put every Components on
        JPanel contents = new JPanel();
        contents.setLayout(new BoxLayout(contents, BoxLayout.Y_AXIS));
        contents.setBackground(Color.decode(SistemAkademikGUI.backgroundPanel));

        // Sets up the title 
        JLabel titleLabel = new JLabel();
        titleLabel.setText(" Tambah Mahasiswa ");
        titleLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);  // Center-align the label in X axis
        
        // Set up "nama" label and its input field 
        JLabel nameLabel = new JLabel();
        nameLabel.setText("Nama: ");
        nameLabel.setHorizontalAlignment(JLabel.CENTER);
        nameLabel.setFont(SistemAkademikGUI.fontGeneral); 
        nameLabel.setAlignmentX(Component.CENTER_ALIGNMENT);  // Center-align the label in X axis

        JTextField namaField = new JTextField(20);
        namaField.setMaximumSize(namaField.getPreferredSize());
        namaField.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        
        // Set up "nama" label and its input field 
        JLabel npmLabel = new JLabel();
        npmLabel.setText("NPM: ");
        npmLabel.setFont(SistemAkademikGUI.fontGeneral);
        npmLabel.setAlignmentX(Component.CENTER_ALIGNMENT);  
        
        JTextField npmField = new JTextField(20);
        npmField.setMaximumSize(npmField.getPreferredSize());
        npmField.setAlignmentX(Component.CENTER_ALIGNMENT);
        

        // Set up "Tambahkan" button and its interactions
        JButton addButton = new JButton("Tambahkan");
        addButton.setFont(SistemAkademikGUI.fontGeneral);
        addButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        addButton.setBackground(SistemAkademikGUI.greenButton);
        addButton.setForeground(SistemAkademikGUI.whiteText);

        // Adds a Mahasiswa instance to daftarMahasiswa
        // if that said instance has not already exists
        addButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                String mahasiswaNama = namaField.getText();  // Nama attribute for mahasiswa instance
                String mahasiswaNPM = npmField.getText();  // NPM attribute for mahasiswa instance
                
                // Validate if atleast one field is empty
                if (mahasiswaNama.length() == 0 || mahasiswaNPM.length() == 0){
                    JOptionPane.showMessageDialog(null,"Mohon isi seluruh Field");
                } else {
                    long npmLong = Long.parseLong(mahasiswaNPM);
                    // Validate if daftarMahasiswa already has
                    // a Mahasiswa with the same name
                    boolean inserted = false;
                    for (Mahasiswa mhs: daftarMahasiswa){
                        if (mhs.getNpm() == npmLong) {
                            inserted = true;
                        }
                    }

                    if (inserted){
                        JOptionPane.showMessageDialog(null,String.format("NPM %s sudah pernah ditambahkan sebelumnya", mahasiswaNPM));
                        namaField.setText(""); 
                        npmField.setText("");
                    } else {
                        
                        daftarMahasiswa.add(new Mahasiswa(mahasiswaNama, npmLong));
                        JOptionPane.showMessageDialog(null,String.format("Mahasiswa %d-%s berhasil ditambahkan", npmLong, mahasiswaNama));
                        namaField.setText(""); 
                        npmField.setText("");
                        
                    }
            } 
        }
        });


        // Set up "Kembali" button and its interactions
        JButton backButton = new JButton("Kembali");
        backButton.setFont(SistemAkademikGUI.fontGeneral);
        backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        backButton.setBackground(SistemAkademikGUI.blueButton);
        backButton.setForeground(SistemAkademikGUI.whiteText);

        // Removes all content/components from current frame
        // and store components from HmeGUI
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){

                frame.getContentPane().removeAll();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            }
        });

        

        // Add the components to JPanel contents
        contents.add(Box.createRigidArea(new Dimension(0,75)));
        contents.add(titleLabel);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(nameLabel);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(namaField);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(npmLabel);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(npmField);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(addButton);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(backButton);

        frame.add(contents);
        frame.setVisible(true);                 


        
    }
    
    
}
