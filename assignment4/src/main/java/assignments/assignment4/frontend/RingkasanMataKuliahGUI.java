package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI {

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        frame.setSize(500,400);

        // JPanel to put every Components on
        JPanel contents = new JPanel();
        contents.setLayout(new BoxLayout(contents, BoxLayout.Y_AXIS));
        contents.setBackground(Color.decode(SistemAkademikGUI.backgroundPanel));

        // Set up the title 
        JLabel titleLabel = new JLabel();
        titleLabel.setText(" Ringkasan Mata Kuliah ");
        titleLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Set up "Pilih Nama Matkul" label
        JLabel matkulLabel = new JLabel();
        matkulLabel.setText("Pilih Nama Matkul");
        matkulLabel.setHorizontalAlignment(JLabel.CENTER);
        matkulLabel.setFont(SistemAkademikGUI.fontGeneral);
        matkulLabel.setAlignmentX(Component.CENTER_ALIGNMENT);


        // List all MataKuliah in a JComboBox menu
        JComboBox<MataKuliah> matkulComboBox = new JComboBox<>();
        matkulComboBox.setSelectedItem(null);    // Default selection
        matkulComboBox.setMaximumSize(new Dimension(100,30));
        matkulComboBox.setFont(SistemAkademikGUI.fontGeneral);

        // Sort daftarMataKuliah
        InsertionSorting.sortArrayList(daftarMataKuliah);

        for (MataKuliah matKul : daftarMataKuliah){
           matkulComboBox.addItem(matKul);
        }

        // Set up the "Lihat" button
        JButton detailButton = new JButton("Lihat");
        detailButton.setFont(SistemAkademikGUI.fontGeneral);
        detailButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        detailButton.setBackground(SistemAkademikGUI.greenButton);
        detailButton.setForeground(SistemAkademikGUI.whiteText);

        // detailButton interactions
        detailButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Validate if null is selected
                if (matkulComboBox.getSelectedItem() == null){
                    JOptionPane.showMessageDialog(null,"Mohon isi seluruh Field");

                } else {
                    // Get the Matakuliah instance 
                    MataKuliah mtkObj = (MataKuliah) matkulComboBox.getSelectedItem();

                    // Jumps to detailRingkasanMahasiswa page
                    frame.getContentPane().removeAll();
                    new DetailRingkasanMataKuliahGUI(frame, mtkObj, daftarMahasiswa, daftarMataKuliah); 
                }
            }
        });

        // Set up "Kembali" button and its interactions
        JButton backButton = new JButton("Kembali");
        backButton.setFont(SistemAkademikGUI.fontGeneral);
        backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        backButton.setBackground(SistemAkademikGUI.blueButton);
        backButton.setForeground(SistemAkademikGUI.whiteText);

        // Removes all content/components from current frame
        // and store components from HmeGUI
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                frame.setSize(500,500);
                frame.getContentPane().removeAll();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            }
        });


        // Add all components to JPanel contents
        contents.add(Box.createRigidArea(new Dimension(0,75)));
        contents.add(titleLabel);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(matkulLabel);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(matkulComboBox);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(detailButton);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(backButton);
        contents.add(Box.createRigidArea(new Dimension(0,10)));

        frame.add(contents);
        frame.setVisible(true);

        
    }

    // Uncomment method di bawah jika diperlukan
    
    // private MataKuliah getMataKuliah(String nama) {

    //     for (MataKuliah mataKuliah : daftarMTK) {
    //         if (mataKuliah.getNama().equals(nama)){
    //             return mataKuliah;
    //         }
    //     }
    //     return null;
    // }
    
}
