package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;


public class TambahIRSGUI {

    public TambahIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        frame.setSize(500,500);
        
        // JPanel to put every Components on
        JPanel contents = new JPanel();
        contents.setLayout(new BoxLayout(contents, BoxLayout.Y_AXIS));
        contents.setBackground(Color.decode(SistemAkademikGUI.backgroundPanel));

        // Set up the title 
        JLabel titleLabel = new JLabel();
        titleLabel.setText(" Tambah IRS ");
        titleLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Set up "Pilih NPM" label
        JLabel npmLabel = new JLabel();
        npmLabel.setText("Pilih NPM");
        npmLabel.setHorizontalAlignment(JLabel.CENTER);
        npmLabel.setFont(SistemAkademikGUI.fontGeneral);
        npmLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
         
        // Set up the JComboBox dropdown menu for listing all NPMs
        JComboBox<Mahasiswa> npmComboBox = new JComboBox<>();
        npmComboBox.setSelectedItem(null);    // Default selection
        npmComboBox.setMaximumSize(new Dimension(150,30));
        npmComboBox.setFont(SistemAkademikGUI.fontGeneral);

        // Sort daftarMahasiswa
        InsertionSorting.sortArrayList(daftarMahasiswa);

        for (Mahasiswa mhsNpm : daftarMahasiswa){
            npmComboBox.addItem(mhsNpm);
        }

        // Set up "Pilih Nama Matkul" label
        JLabel matkulLabel = new JLabel();
        matkulLabel.setText("Pilih Nama Matkul");
        matkulLabel.setHorizontalAlignment(JLabel.CENTER);
        matkulLabel.setFont(SistemAkademikGUI.fontGeneral);
        matkulLabel.setAlignmentX(Component.CENTER_ALIGNMENT);


        // List all MataKuliah in a JComboBox menu
        JComboBox<MataKuliah> matkulComboBox = new JComboBox<>();
        matkulComboBox.setSelectedItem(null);    // Default selection
        matkulComboBox.setMaximumSize(new Dimension(100,30));
        matkulComboBox.setFont(SistemAkademikGUI.fontGeneral);

        // Sort daftarMataKuliah
        InsertionSorting.sortArrayList(daftarMataKuliah);

        for (MataKuliah matKul : daftarMataKuliah){
           matkulComboBox.addItem(matKul);
        }
        
        // Set up the "Tambahkan" button
        JButton addButton = new JButton("Tambahkan");
        addButton.setFont(SistemAkademikGUI.fontGeneral);
        addButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        addButton.setBackground(SistemAkademikGUI.greenButton);
        addButton.setForeground(SistemAkademikGUI.whiteText);

        // addButton interactions
        addButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Validate if at least one field is empty 
                if (npmComboBox.getSelectedItem() == null || matkulComboBox.getSelectedItem() == null){
                    JOptionPane.showMessageDialog(null,"Mohon isi seluruh Field");

                } 
                // Add matakuliah into mahasiswa.daftarMataKuliah array, and 
                // add mahasiswa into matakuliah.daftarMahasiswa array
                // Validations have already been handled in mahasiwa.addMatkul method
                else {
                    // Retrieve Mahasiswa and MataKuliah object
                    Mahasiswa mhsObj = (Mahasiswa) npmComboBox.getSelectedItem();
                    MataKuliah matkulObj = (MataKuliah) matkulComboBox.getSelectedItem();

                    String prompt = mhsObj.addMatkul(matkulObj);
                    JOptionPane.showMessageDialog(null, prompt);
                }
            }
        });
 
        // Set up "Kembali" button and its interactions
        JButton backButton = new JButton("Kembali");
        backButton.setFont(SistemAkademikGUI.fontGeneral);
        backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        backButton.setBackground(SistemAkademikGUI.blueButton);
        backButton.setForeground(SistemAkademikGUI.whiteText);

        // Removes all content/components from current frame
        // and store components from HmeGUI
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){

                frame.getContentPane().removeAll();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            }
        });

        // Add all components to JPanel contents
        contents.add(Box.createRigidArea(new Dimension(0,75)));
        contents.add(titleLabel);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(npmLabel);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(npmComboBox);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(matkulLabel);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(matkulComboBox);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(addButton);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(backButton);
        contents.add(Box.createRigidArea(new Dimension(0,10)));

        frame.add(contents);
        frame.setMinimumSize(contents.getPreferredSize());
        frame.setVisible(true);

    }

    // Uncomment method di bawah jika diperlukan
    
    // private MataKuliah getMataKuliah(String nama) {

    //     for (MataKuliah mataKuliah : daftarMTK) {
    //         if (mataKuliah.getNama().equals(nama)){
    //             return mataKuliah;
    //         }
    //     }
    //     return null;
    // }

    // private Mahasiswa getMahasiswa(long npm) {

    //     for (Mahasiswa mahasiswa : daftarMHS) {
    //         if (mahasiswa.getNpm() == npm){
    //             return mahasiswa;
    //         }
    //     }
    //     return null;
    // }
    
}
