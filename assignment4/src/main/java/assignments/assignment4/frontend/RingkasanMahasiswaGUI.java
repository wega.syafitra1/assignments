package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI {

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        frame.setSize(500,400);

        // JPanel to put every Components on
        JPanel contents = new JPanel();
        contents.setLayout(new BoxLayout(contents, BoxLayout.Y_AXIS));
        contents.setBackground(Color.decode(SistemAkademikGUI.backgroundPanel));

        // Set up the title 
        JLabel titleLabel = new JLabel();
        titleLabel.setText(" Ringkasan Mahasiswa ");
        titleLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        // Set up "Pilih NPM" label
        JLabel npmLabel = new JLabel();
        npmLabel.setText("Pilih NPM");
        npmLabel.setHorizontalAlignment(JLabel.CENTER);
        npmLabel.setFont(SistemAkademikGUI.fontGeneral);
        npmLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
         
        // Set up the JComboBox dropdown menu for listing all NPMs, in String
        JComboBox<Mahasiswa> npmComboBox = new JComboBox<>();
        npmComboBox.setSelectedItem(null);    // Default selection
        npmComboBox.setMaximumSize(new Dimension(150,30));
        npmComboBox.setFont(SistemAkademikGUI.fontGeneral);

        // Sort daftarMahasiswa
        InsertionSorting.sortArrayList(daftarMahasiswa);

        for (Mahasiswa mhsNpm : daftarMahasiswa){
            npmComboBox.addItem(mhsNpm);
        }


        // Set up the "Lihat" button
        JButton detailButton = new JButton("Lihat");
        detailButton.setFont(SistemAkademikGUI.fontGeneral);
        detailButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        detailButton.setBackground(SistemAkademikGUI.greenButton);
        detailButton.setForeground(SistemAkademikGUI.whiteText);

        // detailButton interactions
        detailButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Validate if null is selected
                if (npmComboBox.getSelectedItem() == null){
                    JOptionPane.showMessageDialog(null,"Mohon isi seluruh Field");

                } else {
                    // Get the Mahasiswa instance 
                    Mahasiswa mhsObj = (Mahasiswa) npmComboBox.getSelectedItem();

                    // Jumps to detailRingkasanMahasiswa page
                    frame.getContentPane().removeAll();
                    new DetailRingkasanMahasiswaGUI(frame, mhsObj, daftarMahasiswa, daftarMataKuliah); 
                }
            }
        });

        // Set up "Kembali" button and its interactions
        JButton backButton = new JButton("Kembali");
        backButton.setFont(SistemAkademikGUI.fontGeneral);
        backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        backButton.setBackground(SistemAkademikGUI.blueButton);
        backButton.setForeground(SistemAkademikGUI.whiteText);

        // Removes all content/components from current frame
        // and store components from HmeGUI
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                frame.setSize(500,500);
                frame.getContentPane().removeAll();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            }
        });

        // Add all components to JPanel contents
        contents.add(Box.createRigidArea(new Dimension(0,75)));
        contents.add(titleLabel);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(npmLabel);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(npmComboBox);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(detailButton);
        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(backButton);
        contents.add(Box.createRigidArea(new Dimension(0,10)));

        frame.add(contents);
        frame.setVisible(true);
        
    }

    // Uncomment method di bawah jika diperlukan
    // private Mahasiswa getMahasiswa(long npm) {

    //     for (Mahasiswa mahasiswa : daftarMHS) {
    //         if (mahasiswa.getNpm() == npm){
    //             return mahasiswa;
    //         }
    //     }
    //     return null;
    // }

}
