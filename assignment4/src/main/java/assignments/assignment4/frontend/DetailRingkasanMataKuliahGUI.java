package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI {
    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        frame.setSize(500,500);
        
        // JPanel to put every Components on
        JPanel contents = new JPanel();
        contents.setLayout(new BoxLayout(contents, BoxLayout.Y_AXIS));
        contents.setBackground(Color.decode(SistemAkademikGUI.backgroundPanel));

        // Access the attributes of mataKuliah object
        String nama = mataKuliah.getNama();
        String kode = mataKuliah.getKode();
        int sks = mataKuliah.getSKS();
        int jumMHS = mataKuliah.getJumlahMahasiswa();
        int kapasitas = mataKuliah.getKapasitas();

        // Set up the title 
        JLabel titleLabel = new JLabel();
        titleLabel.setText(" Detail Ringkasan Mata Kuliah ");
        titleLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Set up "Nama" label
        JLabel namaLabel = new JLabel();
        namaLabel.setText(String.format("Nama mata kuliah: %s", nama));  // nama of matakuliah obj
         namaLabel.setFont(SistemAkademikGUI.fontGeneral);
        namaLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        // Set up "Kode" label
        JLabel kodeLabel = new JLabel();
        kodeLabel.setText(String.format("Kode: %s", kode));  // kode of matakuliah obj
        kodeLabel.setFont(SistemAkademikGUI.fontGeneral);
        kodeLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Set up "SKS" label
        JLabel sksLabel = new JLabel();
        sksLabel.setText(String.format("SKS: %d", sks));  // sks of matakuliah obj
        sksLabel.setFont(SistemAkademikGUI.fontGeneral);
        sksLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Set up "Jumlah Mahasiswa" label
        JLabel jumMHSLabel = new JLabel();
        jumMHSLabel.setText(String.format("Jumlah mahasiswa: %d", jumMHS));  // jumMahasisawa of matakuliahobj
        jumMHSLabel.setFont(SistemAkademikGUI.fontGeneral);
        jumMHSLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Set up "Kapasitas" label
        JLabel kapLabel = new JLabel();
        kapLabel.setText(String.format("Kapasitas: %d", kapasitas));  // kapasitas of matakuliah obj
        kapLabel.setFont(SistemAkademikGUI.fontGeneral);
        kapLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Set up "Daftar Mahasiswa" label
        JLabel daftLabel = new JLabel();
        daftLabel.setText("Daftar Mahasiswa: "); 
        daftLabel.setFont(SistemAkademikGUI.fontGeneral);
        daftLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Panel to hold mahasiswa in mataKuliah.daftarMahasiswa array
        JPanel daftMhs = new JPanel();
        daftMhs.setLayout(new BoxLayout(daftMhs, BoxLayout.Y_AXIS));

        // Font for label of mahasiswa.name list
        Font labelListFont = new Font("Century Gothic", Font.BOLD, 12);

        // Validate if there is no mahasiwa in daftarMahasiswa
        if (mataKuliah.getJumlahMahasiswa() == 0){
            JLabel noLabel = new JLabel("Belum ada mata kuliah yang diambil");
            noLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
            daftMhs.add(noLabel);
        } else {

            // Prints mahasiswa name
            for (int i = 0; i < mataKuliah.getJumlahMahasiswa(); i++){
                String mhsName = mataKuliah.getDaftarMahasiswa()[i].getNama();
            

                daftMhs.add(Box.createRigidArea(new Dimension(0,7)));  // Top spacing

                // Create a new JLabel on each iteration
                // listLabel variable for temporary placement of mahasiswa.name
                // Add the new label to daftMhs along with its formatting
                JLabel listLabel = new JLabel(String.format("%d. %s", (i+1),mhsName));
                listLabel.setFont(labelListFont);
                listLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

                daftMhs.add(listLabel);

                daftMhs.add(Box.createRigidArea(new Dimension(0,7)));  // Bottom spacing
            
            }
        }


        // Set up "Selesai" button and its interactions
        JButton backButton = new JButton("Selesai");
        backButton.setFont(SistemAkademikGUI.fontGeneral);
        backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        backButton.setBackground(SistemAkademikGUI.greenButton);
        backButton.setForeground(SistemAkademikGUI.whiteText);

        // Removes all content/components from current frame
        // and store components from HomeGUI
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){

                frame.getContentPane().removeAll();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            }
        });
        // Adds all components to JPanel contents
        contents.add(Box.createRigidArea(new Dimension(0,100)));
        contents.add(titleLabel);
        
        contents.add(Box.createRigidArea(new Dimension(0,15)));
        contents.add(namaLabel);

        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(kodeLabel);

        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(sksLabel);

        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(jumMHSLabel);

        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(kapLabel);

        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(daftLabel);

        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(daftMhs);

        contents.add(Box.createRigidArea(new Dimension(0,10)));
        contents.add(backButton);


        frame.add(contents);
        frame.setVisible(true);
    }
}
