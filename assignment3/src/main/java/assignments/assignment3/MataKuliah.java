package assignments.assignment3;

public class MataKuliah {

    private String nama;
    
    private int kapasitas;

    private Dosen dosen;

    private Mahasiswa[] daftarMahasiswa;

    private int jumMahasiswa;
    
    // Constructor
    public MataKuliah(String nama, int kapasitas) {
        
        this.nama = nama;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    // Getter-Setter
    public String getNamaDosen(){
        if (this.dosen == null){
            return "Belum ada";
        }
        return this.dosen.toString();
    }
    public int getKapasitas(){
        return this.kapasitas;
    }
    public int getJumMahasiswa(){
        return this.jumMahasiswa;
    }
    public Mahasiswa[] getDaftarMahasiswa(){
        return this.daftarMahasiswa;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        // Appends mahasiswa to daftarMahasiswa array
        this.daftarMahasiswa[this.jumMahasiswa] = mahasiswa;
        this.jumMahasiswa++;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        
        for (int i = 0; i < this.kapasitas ; i++){
            // Removes the same mahasiswa as the parameter
            if (mahasiswa.toString().equals(this.daftarMahasiswa[i].toString())){
                // Change the dropped matkul to null
                this.daftarMahasiswa[i] = null;
                // Rearrange daftarmahasiswa
                // Shift the array from the starting point to kapasitas one index to the left
                for (int j = i; j < (this.kapasitas-1); i++){
                    this.daftarMahasiswa[j] = this.daftarMahasiswa[j+1];
                }

                this.jumMahasiswa--;
                break;
            }
        }
    }

    public void addDosen(Dosen dosen) {
        this.dosen = dosen;
    }

    public void dropDosen() {
        this.dosen = null;
    }

    public String toString() {
        return this.nama;
    }
}