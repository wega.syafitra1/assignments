package assignments.assignment3;

public class Mahasiswa extends ElemenFasilkom {
    
    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    
    private long npm;

    private String tanggalLahir;
    
    private String jurusan;

    private int jumMatkul;

    public Mahasiswa(String nama, long npm) {
        
        super(nama, "Mahasiswa");
        this.npm = npm;
    }
    // Getter-Setter
    public long getNPM(){
        return this.npm;
    }
    public MataKuliah[] getDaftarMatkul(){
        return this.daftarMataKuliah;
    }
    public int getJumMatkul(){
        return this.jumMatkul;
    }

    // Custom methods
    public boolean checkMatkul(MataKuliah mataKuliah){
        boolean check = false;
        for (MataKuliah elemen : daftarMataKuliah){
            if (elemen == null) break;
            if (mataKuliah.toString().equals(elemen.toString())) {
                check= true;
                break;
            }
        }
        return check;
    }


    // Required Methods
    public void addMatkul(MataKuliah mataKuliah) {

        // Validates whether mataKuliah has been taken already
        if (checkMatkul(mataKuliah)) {
            System.out.printf("[DITOLAK] %s telah diambil sebelumnya\n", mataKuliah);
        }
        // Validates whether mataKuliah is at full capacity
        else if (mataKuliah.getKapasitas() == mataKuliah.getJumMahasiswa()) {
            System.out.printf("[DITOLAK] %s telah penuh kapasitasnya.\n", mataKuliah);

        } else {
            this.daftarMataKuliah[this.jumMatkul] = mataKuliah;
            this.jumMatkul++;
            mataKuliah.addMahasiswa(this);
            System.out.printf("%s berhasil menambahkan mata kuliah %s\n", this, mataKuliah);
        }
        
    }

    public void dropMatkul(MataKuliah mataKuliah) {
        // Validates if mataKuliah has not been taken before
        
        if (!checkMatkul(mataKuliah)){
            System.out.printf("[DITOLAK] %s belum pernah diambil\n", mataKuliah);
        } else {
            // Removes the mataKuliah from daftarMataKuliah
            for (int i = 0; i < 10 ; i++){
                
                if (mataKuliah.toString().equals(this.daftarMataKuliah[i].toString())){
                    // Drops this mahasiswa from mataKuliah's daftarMahasiswa array
                    mataKuliah.dropMahasiswa(this);
                    
                    // Change the dropped matkul to null
                    this.daftarMataKuliah[i] = null;

                    // Rearrange daftarmahasiswa from the index i
                    for (int j = i; j < (this.jumMatkul-1) ; j++){
                        this.daftarMataKuliah[j] = this.daftarMataKuliah[j+1];
                    }
                    this.jumMatkul--;
                    System.out.printf("%s berhasil drop mata kuliah %s\n", this, mataKuliah);
                    break;
                }
            }
        }
    }

    public String extractTanggalLahir(long npmLong) {
        // Returns tanggal lahir according to the format date-month-year
        String npm = Long.toString(npmLong);
        int biDay = Integer.parseInt(npm.substring(4,6));
        int biMonth = Integer.parseInt(npm.substring(6,8));
        int biYear = Integer.parseInt(npm.substring(8,12));
        return String.format("%s-%d-%d", biDay,biMonth,biYear);
    }

    public String extractJurusan(long npmLong) {
        // Returns jurusan according the majorCode, 01 == Ilmu Komputer, 02 == Sistem Informasi
        String npm = Long.toString(npmLong);
        String majorCode = npm.substring(2,4);
        String majorName = "";
        
        switch (majorCode) {
            case "01":
                majorName = "Ilmu Komputer";
                break;
            case "02":
                majorName = "Sistem Informasi";
                break;
            default:
                break;
            }
        return majorName;
    }  
}