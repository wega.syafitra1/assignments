package assignments.assignment3;

public abstract class ElemenFasilkom implements Comparable<ElemenFasilkom>{
    
    private String tipe;
    
    private String nama;

    private int friendship;

    private ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    private int disapaNum;

    // Constructor
    public ElemenFasilkom(String nama, String tipe){
        this.nama = nama;
        this.tipe = tipe;
    }

    // Getter-Setter

    public String getTipe(){
        return this.tipe;
    }
    public int getFriendship(){
        return this.friendship;
    }
    public int getTotalDisapa(){
        return this.disapaNum;
    }
    public ElemenFasilkom[] getDisapaArr(){
        return this.telahMenyapa;
    }

    public void setFriendship(int friendshipNew){
        // Value of friendship is between 0 and 100
        if (friendshipNew < 0){
            this.friendship = Math.max(this.friendship + friendshipNew, 0);
        } else {
            this.friendship = Math.min(this.friendship + friendshipNew, 100);
        }
    }
    // Custom methods
    public boolean checkTelahMenyapa(ElemenFasilkom elemenPacil){
        // Validate whether elemenFasilkom is in telahMenyapa

        for (ElemenFasilkom elemen : this.telahMenyapa){
            if (elemen == null) break;
            if (elemenPacil.toString().equals(elemen.toString())) {
                return true;
            }
        }
        return false;
    }

    // Handles an interaction case for NEXT_DAY command
    // If Mahasiswa does menyapa Dosen and both have the same MataKuliah, +2 friendship to both
    public void interaksiMahasiswaDosen(Mahasiswa mahasiswa, Dosen dosen){
        if (mahasiswa.getJumMatkul() == 0) return;
        for(MataKuliah matkul : mahasiswa.getDaftarMatkul()){
            // Handles exception error when matkul == null or dosen.mataKuliah == null 
            if (matkul == null) break;
            else if (dosen.getMataKuliah() == null) break;

            else if (matkul.toString().equals(dosen.getMataKuliah().toString())) {
                    mahasiswa.setFriendship(2);
                    dosen.setFriendship(2);
                    break;
            }
        } 
    }
    

    // Required methods
    public void menyapa(ElemenFasilkom elemenFasilkom) {
        if (this.checkTelahMenyapa(elemenFasilkom)) {
            System.out.printf("[DITOLAK] %s telah menyapa %s hari ini\n", this, elemenFasilkom);
        } else {
            // Appends elemenFasilkom to telahMenyapa
            this.telahMenyapa[this.disapaNum] = elemenFasilkom;
            this.disapaNum++;
            // Appends this to elemenFasilkom.telahMenyapa
            elemenFasilkom.telahMenyapa[elemenFasilkom.disapaNum] = this;
            elemenFasilkom.disapaNum++;
            System.out.printf("%s menyapa dengan %s\n", this, elemenFasilkom);
        }
        
        // Mahasiswa and Dosen interaction

        boolean interaction = this.getTipe().equals("Mahasiswa") && elemenFasilkom.getTipe().equals("Dosen");
        boolean interaction2 = this.getTipe().equals("Dosen") && elemenFasilkom.getTipe().equals("Mahasiswa");
        if (interaction == true) {
            Mahasiswa mahasiswa = (Mahasiswa) this;    
            Dosen dosen = (Dosen) elemenFasilkom;
            this.interaksiMahasiswaDosen(mahasiswa, dosen);

        } else if (interaction2 == true){
            Mahasiswa mahasiswa = (Mahasiswa) elemenFasilkom;    
            Dosen dosen = (Dosen) this;
            this.interaksiMahasiswaDosen(mahasiswa, dosen);
        }
            
    }
            
    public void resetMenyapa() {
        // Clears out telahMenyapa
        this.telahMenyapa = new ElemenFasilkom[100];
        this.disapaNum = 0;
    }

    public void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        
        // Try to cast penjual to ElemenKantin
        try {
            ElemenKantin objPenjual = (ElemenKantin) penjual;
            // Search for makanan instance
            Makanan makanan = objPenjual.getMakanan(namaMakanan);

            // Validate if objPembeli doesn't sell namaMakanan
            if (makanan == null){
                System.out.printf("[DITOLAK] %s tidak menjual %s\n", penjual, namaMakanan);
            } else {

                // Handles an interaction case for NEXT_DAY command
                // ElemenFasilkom object does membeliMakanan from ElemenKantin object, +1 friendship to both
                pembeli.setFriendship(1);
                penjual.setFriendship(1);
                System.out.printf("%s berhasil membeli %s seharga %d\n", pembeli, namaMakanan, makanan.getHarga());
            }

        }
        catch (ClassCastException er){
            System.out.printf("[DITOLAK] %s tidak menjual %s\n", penjual, namaMakanan);
        }
    }

    // Comparable method in descending order
    @Override 
    public int compareTo(ElemenFasilkom other){
        if (this.getFriendship() < other.getFriendship()) return 1;
        else if (this.getFriendship() > other.getFriendship()) return -1;
        else {
            // Compare name if friendship is equal
            return this.toString().compareTo(other.toString());
        }
    }

    public String toString() {
        return this.nama;
    }
}