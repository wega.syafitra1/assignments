package assignments.assignment3;


public class ElemenKantin extends ElemenFasilkom {
    
    private Makanan[] daftarMakanan = new Makanan[10];
    private int jumMakanan;

    // Constructor
    public ElemenKantin(String nama) {
        super(nama, "Kantin");
    }

    // Search a makanan by its name
    public Makanan getMakanan(String nama){
        Makanan search = null;
        for (Makanan elemen : this.daftarMakanan){
            if (elemen == null) break;
            else if (elemen.toString().equals(nama)) {
                search = elemen;
                break;
            }
        }
        return search;
    }

    public void setMakanan(String nama, long harga) {
        Makanan obj = getMakanan(nama);
        // Validate whether makanan is in daftarMakanan
        if (obj != null) {
            System.out.printf("[DITOLAK] %s sudah pernah terdaftar\n", nama);
        }
        
        // Appends new makanan instance to daftarMakanan
        else {
            this.daftarMakanan[this.jumMakanan] = new Makanan(nama, harga);
            this.jumMakanan++;
            System.out.printf("%s telah mendaftarkan makanan %s dengan harga %d\n", this, nama, harga);
        }
    }
    
}