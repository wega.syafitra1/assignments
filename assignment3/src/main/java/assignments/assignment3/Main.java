package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Scanner;

public class Main {

    private static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];

    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];

    private static int totalMataKuliah = 0;

    private static int totalElemenFasilkom = 0;

    // Custom Methods

    // Method to search elemenFasilkom by its name
    public static ElemenFasilkom getElemenFasilkom(String objectName){
        ElemenFasilkom search = null;
        for (ElemenFasilkom elemen : daftarElemenFasilkom){
            if (elemen == null) break;
            if (elemen.toString().equals(objectName)){
                search = elemen;
                break;
            }
        }
        return search;
    }
    // Method to search mataKuliah by its name
    public static MataKuliah getMataKuliah(String objectName){
        MataKuliah search = null;
        for (MataKuliah elemen : daftarMataKuliah){
            if (elemen == null) break;
            if (elemen.toString().equals(objectName)){
                search = elemen;
                break;
            }
        }
        return search;
    }


    // Required Methods

    public static void addMahasiswa(String nama, long npm) {
        daftarElemenFasilkom[totalElemenFasilkom] = new Mahasiswa(nama, npm);
        totalElemenFasilkom++;
        System.out.printf("%s berhasil ditambahkan\n", nama);
        
    }

    public static void addDosen(String nama) {
        daftarElemenFasilkom[totalElemenFasilkom] = new Dosen(nama);
        totalElemenFasilkom++;
        System.out.printf("%s berhasil ditambahkan\n", nama);
        
    }

    public static void addElemenKantin(String nama) {
        daftarElemenFasilkom[totalElemenFasilkom] = new ElemenKantin(nama);
        totalElemenFasilkom++;
        System.out.printf("%s berhasil ditambahkan\n", nama);
       
    }

    public static void menyapa(String objek1, String objek2) {
        ElemenFasilkom elemenObj1 = getElemenFasilkom(objek1);
        ElemenFasilkom elemenObj2 = getElemenFasilkom(objek2);

        // Validate if both objek1 and objek2 is the same
        if (objek1.equals(objek2)) {
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa\n");
        }
        else {
            elemenObj1.menyapa(elemenObj2);
        }
    }

    public static void addMakanan(String objek, String namaMakanan, long harga) {
        
        ElemenFasilkom elemenObjek = getElemenFasilkom(objek);

        // Checks if objek is an ElemenKantin
        if (elemenObjek.getTipe().equals("Kantin")){
            ElemenKantin objekKantin = (ElemenKantin) elemenObjek;
            objekKantin.setMakanan(namaMakanan, harga);
        } else {
            System.out.printf("[DITOLAK] %s bukan merupakan elemen kantin\n", objek);
        }

    }

    public static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        
        ElemenFasilkom elemenObj1 = getElemenFasilkom(objek1);
        ElemenFasilkom elemenObj2 = getElemenFasilkom(objek2);
        
        // Validate if elemenObj2 is not an ElemenKantin
        if (!elemenObj2.getTipe().equals("Kantin")){
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
        // Validate if both are ElemenKantin
        } else if (elemenObj2.getTipe().equals("Kantin") && objek1.equals(objek2)){
            System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
        } else {
            elemenObj1.membeliMakanan(elemenObj1, elemenObj2, namaMakanan);
        }

    }

    public static void createMatkul(String nama, int kapasitas) {
        daftarMataKuliah[totalMataKuliah] = new MataKuliah(nama, kapasitas);
        totalMataKuliah++;
        System.out.printf("%s berhasil ditambahkan dengan kapasitas %d\n", nama, kapasitas);
    }

    public static void addMatkul(String objek, String namaMataKuliah) {
        
        ElemenFasilkom elemenObj = getElemenFasilkom(objek);
        // Validate if objek is not a Mahasiswa 
        if (!elemenObj.getTipe().equals("Mahasiswa")){
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
        } else {
            Mahasiswa mahasiswaObj = (Mahasiswa) elemenObj;
            MataKuliah matkulObj = getMataKuliah(namaMataKuliah);
            mahasiswaObj.addMatkul(matkulObj);
        }
        
    }

    public static void dropMatkul(String objek, String namaMataKuliah) {

        ElemenFasilkom elemenObj = getElemenFasilkom(objek);
        // Validate if objek is not a Mahasiswa 
        if (!elemenObj.getTipe().equals("Mahasiswa")){
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
        } else {
            Mahasiswa mahasiswaObj = (Mahasiswa) elemenObj;
            MataKuliah matkulObj = getMataKuliah(namaMataKuliah);
            mahasiswaObj.dropMatkul(matkulObj);
        }
        
    }

    public static void mengajarMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom elemenObj = getElemenFasilkom(objek);
        // Validate if objek is not a Dosen
        if (!elemenObj.getTipe().equals("Dosen")){
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
        } else {
            Dosen dosenObj = (Dosen) elemenObj;
            MataKuliah matkulObj = getMataKuliah(namaMataKuliah);
            dosenObj.mengajarMataKuliah(matkulObj);
        }
        
    }

    public static void berhentiMengajar(String objek) {
        ElemenFasilkom elemenObj = getElemenFasilkom(objek);

        // Validate if objek is not a Dosen
        if (!elemenObj.getTipe().equals("Dosen")){
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
        } else {
            Dosen dosenObj = (Dosen) elemenObj;
            dosenObj.dropMataKuliah();
        }
        
    }

    public static void ringkasanMahasiswa(String objek) {
        ElemenFasilkom elemenObj = getElemenFasilkom(objek);

        // Validate if objek is a Mahasiwa
        if (elemenObj.getTipe().equals("Mahasiswa")) {
            Mahasiswa mahasiswa = (Mahasiswa) getElemenFasilkom(objek);
            System.out.println("Nama: " + mahasiswa);
            System.out.println("Tanggal lahir: " + mahasiswa.extractTanggalLahir(mahasiswa.getNPM()));
            System.out.println("Jurusan: " + mahasiswa.extractJurusan(mahasiswa.getNPM()));
            System.out.println("Daftar Mata Kuliah:");
            
            // Validate if mahasiswa hasn't taken any MataKuliah
            if (mahasiswa.getJumMatkul() == 0) {
                System.out.println("Belum ada mata kuliah yang diambil");
            } else {
                // Prints every matakuliah that a mahasiswa has taken
                for (int i = 0; i< mahasiswa.getJumMatkul() ; i++){
                    System.out.println(String.valueOf(i+1) + ". " + mahasiswa.getDaftarMatkul()[i]);
                }
            }
        }
        else {
            System.out.printf("[DITOLAK] %s bukan merupakan seorang mahasiswa\n", objek);
        }
    }

    public static void ringkasanMataKuliah(String namaMataKuliah) {

        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
        
        System.out.println("Nama mata kuliah: " + mataKuliah.toString());
        System.out.println("Jumlah mahasiswa: " + mataKuliah.getJumMahasiswa());
        System.out.println("Kapasitas: " + mataKuliah.getKapasitas());
        System.out.println("Dosen pengajar: " + mataKuliah.getNamaDosen());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini:");
       
        // Validates if no mahasiwa has taken this mataKuliah yet
        if (mataKuliah.getJumMahasiswa() == 0){
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
        } else {
            // Print every mahasiswa
            for (int i = 0; i < mataKuliah.getJumMahasiswa(); i++){
                System.out.println(String.valueOf(i+1) + ". " + mataKuliah.getDaftarMahasiswa()[i]);
            }
        }
    }

    public static void nextDay() {
        int constant = ((totalElemenFasilkom-1)/2);

        // Iterates for every non-null instance in daftarElemenFasilkom array
        for (ElemenFasilkom elemen : daftarElemenFasilkom){

            if (elemen == null) break;
            else {
                // ElemenFasilkom does menyapa with more or equal than half of totalElemenFasilkom, excluding itself
                if (elemen.getTotalDisapa() >= constant) elemen.setFriendship(10);
                // Handles if it doesn't
                else elemen.setFriendship(-5);
                
            }
            // Reset talahMenyapa array for every ElemenFasilkom in daftarElemenFasilkom
            elemen.resetMenyapa();
        }
        
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        friendshipRanking();
    }

    public static void friendshipRanking() {
        // Converts daftarElemenFasilkom to a List, sort it based on friendship attribute;
        List<ElemenFasilkom> lst = new ArrayList<ElemenFasilkom>();
        for (int i =0; i < totalElemenFasilkom; i++){
            lst.add(daftarElemenFasilkom[i]);
        }
        Collections.sort(lst);

        // Prints every elemenFasilkom
        for (int i = 0; i < totalElemenFasilkom; i++){
            ElemenFasilkom obj = lst.get(i);
            System.out.printf("%d. %s(%d)\n", (i+1), obj.toString(), obj.getFriendship());
        }
    }

    public static void programEnd() {
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        friendshipRanking();
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        } input.close();
    }
}

/* 
In collaboration with :

Alya Muthia Fitri
Andrea Debora Narulita
Atika Najla Febryani 
Raissa Adlina Febrianny 
*/