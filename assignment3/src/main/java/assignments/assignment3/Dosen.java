package assignments.assignment3;

public class Dosen extends ElemenFasilkom {

    private MataKuliah mataKuliah;

    // Constructor
    public Dosen(String nama) {
        super(nama, "Dosen");
    }

    // Getter-Setter
    public MataKuliah getMataKuliah(){
        return this.mataKuliah;
    }

    public void mengajarMataKuliah(MataKuliah mataKuliah) {
       
        // Validate if dosen is lecturing a certain mataKuliah 
        if (this.mataKuliah != null){
            System.out.printf("[DITOLAK] %s sudah mengajar mata kuliah %s\n", this, this.mataKuliah);
    
        // Validate mataKuliah is being lectured by someone else
        } else if (!mataKuliah.getNamaDosen().equals("Belum ada")){
            System.out.printf("[DITOLAK] %s sudah memiliki dosen pengajar\n", mataKuliah);

        } else {
            mataKuliah.addDosen(this);
            this.mataKuliah = mataKuliah;
            System.out.printf("%s mengajar mata kuliah %s\n", this, mataKuliah);
        }
    }

    public void dropMataKuliah() {
        // Validate if dosen is not lecturing any mataKuliah
        if (this.mataKuliah == null) {
            System.out.printf("%s sedang tidak mengajar mata kuliah apapun\n", this); 
        }

        else {
            System.out.printf("%s berhenti mengajar %s\n", this, this.mataKuliah);
            this.mataKuliah.dropDosen();
            this.mataKuliah = null;
        }
    }
}