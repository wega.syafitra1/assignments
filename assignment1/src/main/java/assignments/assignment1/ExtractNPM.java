package assignments.assignment1;

import java.util.*;

public class ExtractNPM {
    // Main method to handle user inputs
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }

            // Conditionals to validate and extract NPM 
            if (validate(npm) == true){
                // Extract NPM if it's valid
                System.out.println(extract(npm));

            } else {
               System.out.println("NPM Tidak Valid!");
            }
        }
        //System.out.println(calculateCodeNPM(150));
        input.close(); 
    }

    // Method to validate NPM
    public static boolean validate(long npm) {
        /**
         * Four required conditions : 
         * 1) Only 14 digits long
         * 2) Member of one of 5 available majors
         * 3) Older than 15 years old
         * 4) NPM code is valid according to calculations
         */
        
        // Check the length, must be 14 digits long
        if (String.valueOf(npm).length() == 14) {

            // Invoke the method to validate major
            if (majorCode(npm)== true){

                // Invoke the method to validate age
                if (ageCheck(npm) == true) {

                    // Take the 1st through the 13th index to calculate and validate NPM code
                    String npmStr = String.valueOf(npm);
                    long npmCalculate = Long.parseLong(npmStr.substring(0,13));
                    int currentCode = Integer.parseInt(String.valueOf(npmStr.charAt(13)));

                    if (verifyCodeNPM(npmCalculate, currentCode) == true) {

                        // Passes through every validation check
                        return true;
                    }

                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
        return false;
    }
    
    // Method to check majors
    public static boolean majorCode(long npm){
        String checkedStr = Long.toString(npm).substring(2,4);
        String[] arrayJurusan = {"01","02","03","11","12"}; 
        boolean bool = true;
        for (String element : arrayJurusan) {
            if (element.equals(checkedStr)) {
                bool = true;
                break;
            } else {
                bool = false;
            }
        }
        return bool;
    }

    // Method to check whether the age is older than 15
    public static boolean ageCheck(long npm) {
        boolean bool = true;
        // Convert npm into string
        String birthStr = Long.toString(npm).substring(8,12);
        String entryStr = "20" + Long.toString(npm).substring(0,2);

        // Convert back into int
        int birthInt = Integer.parseInt(birthStr);
        int entryInt = Integer.parseInt(entryStr);
        
        // Check difference between entry year and birth year
        if ((entryInt - birthInt) >= 15){
            bool = true;
        } else {
            bool = false;
        }
        return bool;

    }

    // Method to calculate the NPM code
    public static int calculateCodeNPM(long npm){

        // Convert npm into an array, with every element corresponds to an npm digit
        ArrayList<Integer> lst = new ArrayList<Integer>();
        String npmStr = Long.toString(npm);
        for(int i = 0; i < npmStr.length(); i++){
            lst.add((int) npmStr.charAt(i) - '0');
        }
        // Create a reversed duplicate array
        ArrayList<Integer> lstReverse = new ArrayList<>(lst);
        Collections.reverse(lstReverse);

        // Multiply each corresponding indices of the array and its reverse
        // and add that to the final sum

        int multiply = 0;
        int sum = 0;
        
        for(int i = 0; i < (lst.size()/2); i++){
                multiply = (int) (lst.get(i)* lstReverse.get(i));
                sum += multiply;
        }
        return sum + lst.get(6);
    }

    // Method to verify whether the NPM code is correct
    public static boolean verifyCodeNPM (Long npm, int code){
        boolean check = true;
        int sum = 0;
        sum = calculateCodeNPM(npm);

        // Keep recalculating the NPM code using the sum until it's less than 10
        while (sum >= 10) {
            if (sum < 100) {
                // Add the tens and the ones
                sum = ((sum/10) + (sum%10));
            } else {
                sum = 0;
                //sum = calculateCodeNPM(sum);
                String sumStr = Integer.toString(sum);
                for (int i = 0; i < sumStr.length(); i++){
                    sum += Integer.parseInt(sumStr.substring(i, i+1));
                }
                
            }
        }

        // Verify the NPM
        if (sum == code) {
            check = true;
        } else {
            check = false;
        }

        return check;
    }

    // Method to extract information from NPM with given format
    public static String extract(long npmLong) {
        
        String npm = Long.toString(npmLong);
        String thnMasuk = npm.substring(0,2);
        String majorCode = npm.substring(2,4);
        String biDay = npm.substring(4,6);
        String biMonth = npm.substring(6,8);
        String biYear = npm.substring(8,12);
        String majorName = "";

        // Switch cases for each different major codes
        switch (majorCode) {
            case "01":
                majorName = "Ilmu Komputer";
                break;
            case "02":
                majorName = "Sistem Informasi";
                break;
            case "03":
                majorName = "Teknologi Informasi";
                break;
            case "11":
                majorName = "Teknik Telekomunikasi";
                break;
            case "12":
                majorName = "Teknik Elektro";
                break;
            default :
                majorName = "";
                break;
            
        }
        
        String output = "Tahun masuk: " + "20" + thnMasuk +  "\n" + 
                        "Jurusan: " + majorName + "\n" +
                        "Tanggal Lahir: " + biDay +  '-' + biMonth +  '-' + biYear ;
        return output;
    }

}


