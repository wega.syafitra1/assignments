package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;
    private int mahasiswaNum;

    // Constructor
    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    // Getter methods 
    public String getKode(){
        return this.kode;
    }
    public int getKapasitas() {
        return this.kapasitas;
    }
    public int getSks(){
        return this.sks;
    }
    public int getJumMahasiswa(){
        return this.mahasiswaNum;
    }
    public Mahasiswa getSpesMahasiswa(int ind){
        return this.daftarMahasiswa[ind];
    }
    
    public void addMahasiswa(Mahasiswa mahasiswa) {
        
        this.daftarMahasiswa[this.mahasiswaNum] = mahasiswa;
        this.mahasiswaNum++;

    }
    // Shift the array from the starting point to kapasitas 
    // one index to the left
    public Mahasiswa[] rearrange(Mahasiswa[] array, int startIndex){
        for (int i = startIndex; i < (this.kapasitas-1); i++){
            array[i] = array[i+1];
        }

        return array;
    }
    public void dropMahasiswa(Mahasiswa mahasiswa) {
        for (int i = 0; i < this.kapasitas ; i++){
            // Removes the same mahasiswa as the parameter
            if (mahasiswa.toString().equals(this.daftarMahasiswa[i].toString())){
                // Change the dropped matkul to null
                this.daftarMahasiswa[i] = null;
                // Rearrange daftarmahasiswa with the method
                this.daftarMahasiswa = rearrange(this.daftarMahasiswa, i);
                this.mahasiswaNum--;
                break;
            }
        }
    }

    // Returns nama as a String
    public String toString() {
        return this.nama;
    }
}
