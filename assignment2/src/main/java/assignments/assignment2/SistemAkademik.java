package assignments.assignment2;

import java.util.Scanner;

public class SistemAkademik {
    private static final int ADD_MATKUL = 1;
    private static final int DROP_MATKUL = 2;
    private static final int RINGKASAN_MAHASISWA = 3;
    private static final int RINGKASAN_MATAKULIAH = 4;
    private static final int KELUAR = 5;
    private static Mahasiswa[] daftarMahasiswa = new Mahasiswa[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    
    private Scanner input = new Scanner(System.in);

    // Find a mahasiswa with a particular npm
    // Npm is unique for each mahasiswa

    private Mahasiswa getMahasiswa(long npm) {
        // Iterate over each mahasiswa in daftarMahasiswa array

        Mahasiswa search = null;
        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa == null) break;
            if (mahasiswa.getNPM() == npm){
                search = mahasiswa;
            }
        }
        return search;
    }
    // Find a mataKuliah by its name
    // namaMataKuliah is unique for each mataKuliah
    private MataKuliah getMataKuliah(String namaMataKuliah) {
        // Iterate over each mataKuliah in daftarMataKuliah array

        MataKuliah search = null;
        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah == null) break;
            if (mataKuliah.toString().equals(namaMataKuliah)){
                search = mataKuliah;
            }
        }
        return search;
    }

    private void addMatkul(){
        System.out.println("\n--------------------------ADD MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");
        long npm = Long.parseLong(input.nextLine());

        // Find a mahasiswa instance by npm
        Mahasiswa mahasiswa = getMahasiswa(npm);
        

        System.out.print("Banyaknya Matkul yang Ditambah: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        
        System.out.println("Masukkan nama matkul yang ditambah");
        for(int i=0; i<banyakMatkul; i++){
            System.out.print("Nama matakuliah " + i+1 + " : ");
            String namaMataKuliah = input.nextLine();
            // Add an instance of matakuliah
            MataKuliah matkulAdd = getMataKuliah(namaMataKuliah);

            // Validate whether matkul has been taken already
            if (mahasiswa.isMatkulTaken(namaMataKuliah) == true) {
                System.out.println("[DITOLAK] " + namaMataKuliah + " telah diambil sebelumnya.");
            }

            // Validate whether matkul is full
            else if (matkulAdd.getKapasitas() == matkulAdd.getJumMahasiswa()) {
                System.out.println("[DITOLAK] " + namaMataKuliah + " telah penuh kapasitasnya.");
            }
            
            // Validate amount of matkul taken
            else if (mahasiswa.getJumMatkul() > 9){
                System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.");
                
            }
            else {
            
                // Insert mataKuliah to matakuliah array of mahasiswa
                // +1 to mahasiswaNum of MataKuliah instance
                // +1 to matkulnum of Mahasiswa instance
                matkulAdd.addMahasiswa(mahasiswa);
                mahasiswa.addMatkul(matkulAdd);}
            

        }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    private void dropMatkul(){
        System.out.println("\n--------------------------DROP MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan DROP MATKUL : ");
        long npm = Long.parseLong(input.nextLine());
        
        // Find a mahasiswa by its npm and assigns to a variable
        Mahasiswa mahasiswa = getMahasiswa(npm);
        // Handling if mahasiswa hasn't added any matakuliah
        if (mahasiswa.getJumMatkul() == 0){
            System.out.println("[DITOLAK] Belum ada mata kuliah yang diambil.");
            return;}

        System.out.print("Banyaknya Matkul yang Di-drop: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan nama matkul yang di-drop:");

        for(int i=0; i<banyakMatkul; i++){
            System.out.print("Nama matakuliah " + i+1 + " : ");
            String namaMataKuliah = input.nextLine();

            
            // Handling if mahasiswa hasn't taken this spesific namamatakuliah
            if (mahasiswa.isMatkulTaken(namaMataKuliah) == false) {
                System.out.println("[DITOLAK] " + namaMataKuliah + " belum pernah diambil."); 
                break;

            } else {
                MataKuliah matkulDropped = getMataKuliah(namaMataKuliah); // Find a matkul
                mahasiswa.dropMatkul(matkulDropped);
                matkulDropped.dropMahasiswa(mahasiswa);
            }

        }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    private void ringkasanMahasiswa(){
        System.out.print("Masukkan npm mahasiswa yang akan ditunjukkan ringkasannya : ");
        long npm = Long.parseLong(input.nextLine());
        
        // Isi sesuai format keluaran
        Mahasiswa mahasiswa = getMahasiswa(npm);
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama: " + mahasiswa.toString());
        System.out.println("NPM: " + npm);
        System.out.println("Jurusan: " + mahasiswa.getJurusan());
        System.out.println("Daftar Mata Kuliah: ");

        // Handle kasus jika belum ada mata kuliah yang diambil
        if (mahasiswa.getJumMatkul() == 0) {
            System.out.println("Belum ada mata kuliah yang diambil");
        } else {
            // Prints every matakuliah that a mahasiswa has taken
            for (int i = 0; i< mahasiswa.getJumMatkul() ; i++){
                System.out.println(String.valueOf(i+1) + ". " + mahasiswa.getSpesMataKuliah(i));
            }
        }

        System.out.println("Total SKS: " + mahasiswa.getTotalSKS());
        
        System.out.println("Hasil Pengecekan IRS:");
        // Invoke the method of cekIRS
        mahasiswa.cekIRS();
        
        // Handles if there's no problem
        if (mahasiswa.getTotalIRSproblem() == 0) {
            System.out.println("IRS tidak bermasalah.");
        } else {
            // Prints every string in masalahIRS array
            for (int i = 0; i < mahasiswa.getTotalIRSproblem(); i++) {
                System.out.println((i+1) + ". " + mahasiswa.getMasalahIRS()[i]);
                
            }
        }

        // Reset masalahIRS array after every checking procedure
        mahasiswa.setMasalahIRS(new String[19]);
        mahasiswa.setJumMasalah(0);
        
    }

    private void ringkasanMataKuliah(){
        System.out.print("Masukkan nama mata kuliah yang akan ditunjukkan ringkasannya : ");
        String namaMataKuliah = input.nextLine();
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
        
        // Isi sesuai format keluaran
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama mata kuliah: " + mataKuliah.toString());
        System.out.println("Kode: " + mataKuliah.getKode());
        System.out.println("SKS: " + mataKuliah.getSks());
        System.out.println("Jumlah mahasiswa: " + mataKuliah.getJumMahasiswa());
        System.out.println("Kapasitas: " + mataKuliah.getKapasitas());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");
       
        // Handle kasus jika tidak ada mahasiswa yang mengambil */
        if (mataKuliah.getJumMahasiswa() == 0){
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini.");
        } else {
            // Print every mahasiswa
            for (int i = 0; i < mataKuliah.getJumMahasiswa(); i++){
                System.out.println(String.valueOf(i+1) + ". " + mataKuliah.getSpesMahasiswa(i));
            }
        }
        
    }

    private void daftarMenu(){
        int pilihan = 0;
        boolean exit = false;
        while (!exit) {
            System.out.println("\n----------------------------MENU------------------------------\n");
            System.out.println("Silakan pilih menu:");
            System.out.println("1. Add Matkul");
            System.out.println("2. Drop Matkul");
            System.out.println("3. Ringkasan Mahasiswa");
            System.out.println("4. Ringkasan Mata Kuliah");
            System.out.println("5. Keluar");
            System.out.print("\nPilih: ");
            try {
                pilihan = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                continue;
            }
            System.out.println();
            if (pilihan == ADD_MATKUL) {
                addMatkul();
            } else if (pilihan == DROP_MATKUL) {
                dropMatkul();
            } else if (pilihan == RINGKASAN_MAHASISWA) {
                ringkasanMahasiswa();
            } else if (pilihan == RINGKASAN_MATAKULIAH) {
                ringkasanMataKuliah();
            } else if (pilihan == KELUAR) {
                System.out.println("Sampai jumpa!");
                exit = true;
            }
        }
    }


    private void run() {
        System.out.println("====================== Sistem Akademik =======================\n");
        System.out.println("Selamat datang di Sistem Akademik Fasilkom!");
        
        System.out.print("Banyaknya Matkul di Fasilkom: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan matkul yang ditambah");
        System.out.println("format: [Kode Matkul] [Nama Matkul] [SKS] [Kapasitas]");

        for(int i=0; i<banyakMatkul; i++){
            // Takes input from user for matakuliah
            String[] dataMatkul = input.nextLine().split(" ", 4);

            int sks = Integer.parseInt(dataMatkul[2]);
            int kapasitas = Integer.parseInt(dataMatkul[3]);

            // Create matakuliah instance and insert to daftarMataKuliah Array
            daftarMataKuliah[i] = new MataKuliah(dataMatkul[0], dataMatkul[1], sks, kapasitas);
        }

        System.out.print("Banyaknya Mahasiswa di Fasilkom: ");
        int banyakMahasiswa = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan data mahasiswa");
        System.out.println("format: [Nama] [NPM]");

        for(int i=0; i<banyakMahasiswa; i++){
            // Takes input from user for mahasiswa
            String[] dataMahasiswa = input.nextLine().split(" ", 2);

            long npm = Long.parseLong(dataMahasiswa[1]);

            // Create mahasiswa instance and insert to daftarMataKuliah array
            daftarMahasiswa[i] = new Mahasiswa(dataMahasiswa[0], npm);
        }

        daftarMenu();
        input.close();
    }

    public static void main(String[] args) {
        SistemAkademik program = new SistemAkademik();
        program.run();
    }


    
}
/* 
>>> In collaboration with :
Alya Muthia Fitri
Andrea Debora Narulita
Atika Najla Febryani 
Raissa Adlina Febrianny 


*/