package assignments.assignment2;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private String[] masalahIRS = new String[19];
    private int totalSKS;
    private String nama;
    private String jurusan; // Kode jurusan
    private long npm;
    private int matKulNum;
    private int problemIRSnum;

    // Contructor
    public Mahasiswa(String nama, long npm){
        this.nama = nama;
        this.npm = npm;
        this.jurusan = String.valueOf(npm).substring(2,4);
        
    }
    
    // Getter methods
    public long getNPM() {
        return this.npm;
    }

    // Returns jurusan as an unabbreviated string
    public String getJurusan(){
        String out = "";
        // Two types of jurusan
        switch(this.jurusan) {
            case "01":
                out = "Ilmu Komputer";
                break;
            case "02":
                out = "Sistem Informasi";
                break;
        }
        return out;
    }

    // Returns jurusan in its abbreviated form
    public String getJurShorted(){
        String shorted = "";
        switch(this.jurusan) {
            case "01":
                shorted = "IK";
                break;
            case "02":
                shorted = "SI";
                break;
        }
        return shorted;
    }

    public int getJumMatkul(){
        return this.matKulNum;
    }
    public MataKuliah getSpesMataKuliah(int ind){
        return this.mataKuliah[ind];
    }
    public int getTotalSKS(){
        return this.totalSKS;
    }
    public int getTotalIRSproblem(){
        return this.problemIRSnum;
    }
    public String[] getMasalahIRS(){
        return this.masalahIRS;
    }

    // Setter 
    public void setMasalahIRS(String[] arr){
        this.masalahIRS = arr; 
    }
    public void setJumMasalah (int set) {
        this.problemIRSnum = set;
    }


    // Add matKulAdd to mataKuliah array
    public void addMatkul(MataKuliah matKulAdd){
        
        this.mataKuliah[this.matKulNum] = matKulAdd;
        this.matKulNum++;
        this.totalSKS += matKulAdd.getSks();
    }

    // Removes matkulDrop from matakuliah array
    public void dropMatkul(MataKuliah matKulDrop){

        for (int i = 0; i < 10 ; i++){
            // Rearrange matakuliah array
            if (matKulDrop.toString().equals(this.mataKuliah[i].toString())){
                // Rearrange using the method
                this.mataKuliah = rearrange(this.mataKuliah, i);
                this.matKulNum--;
                this.totalSKS -= matKulDrop.getSks();
                break;
            }
        }
    }

    // Works similarly to rearrange method in matakuliah class
    // Shifts all element one index to the left from starting point
    public MataKuliah[] rearrange(MataKuliah[] array, int startIndex){
        for (int i = startIndex; i < (this.matKulNum -1) ; i++){
            array[i] = array[i+1];
        }
        return array;
    }
    
    // Check whether a mahasiswa has taken a spesific matkul
    public boolean isMatkulTaken(String namaMatkul){
        boolean check = false;
        for (MataKuliah taken : this.mataKuliah) {
            if (taken == null) break;
            if (taken.toString().equals(namaMatkul)){
                check = true;
            }
        }
        return check;
    }

    public void cekIRS(){

        // totalSKS must be at max 24
        if (this.totalSKS > 24 ) {
            // Adds string to the array
            this.masalahIRS[this.problemIRSnum] = "SKS yang Anda ambil lebih dari 24"; 
            this.problemIRSnum++;
           
        } 

        // Validate every matkul taken
        // Adds string to the masalahIRS array if doesn't pass the check
        for (MataKuliah matkul : this.mataKuliah) {
            if (isMatkulCorrect(matkul) == false) {
                this.masalahIRS[this.problemIRSnum] = "Mata Kuliah " + matkul.toString() 
                                                    + " tidak dapat diambil jurusan " + getJurShorted(); 
                this.problemIRSnum++;
            }
        }
    }


    // Check whether one matakuliah taken is approriate for the jurusan
    // Ilmu komputer can take matkul with IK or CS code
    // Sistem Informasi can take matkul with SI or CS code
     
    public boolean isMatkulCorrect(MataKuliah matkulCheck){
        boolean check = true;
        if (matkulCheck == null) return true;

        // CS code matkul can be taken by every mahasiswa  
        if (matkulCheck.getKode().equals("CS")) check = true;
        // Correct if matakuliah.kode is the same as jurusan in shorted form
        else if (getJurShorted().equals(matkulCheck.getKode())) check = true;

        else check = false;
        
        return check;
    }

    
    public String toString() {
        // Returns nama as string
        return this.nama;
    }

}
